# Ctenophore transcriptome data from: #

## Transcriptomes include: ##

* [*Hormiphora californensis*](http://www.ncbi.nlm.nih.gov/biosample/SAMN03539694) Trinity assembly at NCBI [GGLO01](https://www.ncbi.nlm.nih.gov/Traces/wgs/GGLO01), [normalized](https://bitbucket.org/wrf/ctenophore-transcriptomes/downloads/Hormiphora_californensis_9110X13_Trinity.fasta.gz), reads at [SRR1992642](http://www.ncbi.nlm.nih.gov/sra/SRX1006956). This sample is asserted to be *H. californesis*, though does not group with other samples/genome, and may be *H. palmata* or another species.

Francis, W.R., N.C. Shaner, L.M. Christianson, M.L. Powers, and S.H.D. Haddock (2015) [Occurrence of Isopenicillin-N-Synthase homologs in bioluminescent ctenophores and implications for coelenterazine biosynthesis](https://www.ncbi.nlm.nih.gov/pubmed/26125183). **PLOS ONE** 10:6

Ryan, J.F., et al. (2013) [The genome of the ctenophore Mnemiopsis leidyi and its implications for cell type evolution](http://www.ncbi.nlm.nih.gov/pubmed/24337300). **Science** 342:6164

* Data from [Simion 2017](http://dx.doi.org/10.1016/j.cub.2017.02.031), the assemblies do not appear to be [on their github page](https://github.com/psimion/SuppData_Metazoa_2017) so I have put them [in the downloads tab](https://bitbucket.org/wrf/ctenophore-transcriptomes/downloads/). There may be contamination between samples (i.e. between cnidarians, ctenophores, and sponges in this dataset), as the authors created a pipeline to examine this, see [Simion 2018](https://doi.org/10.1186/s12915-018-0486-7).

## Notes: ##
* Sample of [*Pukia falcata*](https://neurobase.rc.ufl.edu/pleurobrachia/download) from [Whelan 2017](http://dx.doi.org/10.1038/s41559-017-0331-3) appears to be contaminated with an arthropod (probably crustacean), likely from gut contents

# Species index #
| Species               | Group      | Subgroup   | Type   | Raw data | Assembly | Size | Seqs | N50 | Reference |
|-----------------------|------------|------------|--------|----------|----------|--:|-----:|----:|-----------|
| [*Euplokamis dunlapae*](http://www.marinespecies.org/aphia.php?p=taxdetails&id=265174) |  |  | Txome |  | [illumina](https://neurobase.rc.ufl.edu/pleurobrachia/download) |  |  |  | [Moroz 2014](https://doi.org/10.1038/nature13400) |
| *Euplokamis* sp "Gulf 2" |  |  | Txome |  | [trinity](https://neurobase.rc.ufl.edu/pleurobrachia/download) |  | 48729 |  | - |
| [*Vampyroctena delmarvensis*](https://marinespecies.org/aphia.php?p=taxdetails&id=1435019) | Cydippida | Vampyroctenidae | Txome | - | [Trinity](https://doi.org/10.6084/m9.figshare.6771635.v1) |  | 172671 |  | [Townsend 2020](https://doi.org/10.1007/s12526-020-01049-9) |
| [*Coeloplana astericola*](http://www.marinespecies.org/aphia.php?p=taxdetails&id=265859) | Platyctenida | Coeloplanidae | Txome |  | [illumina](https://neurobase.rc.ufl.edu/pleurobrachia/download) |  |  |  | [Moroz 2014](https://doi.org/10.1038/nature13400) |
| [*Coeloplana meteoris*](http://www.marinespecies.org/aphia.php?p=taxdetails&id=559367) | Platyctenida | Coeloplanidae | Txome | [SRR3407215](https://www.ncbi.nlm.nih.gov/sra/SRX1716870) | [illumina](https://bitbucket.org/wrf/ctenophore-transcriptomes/downloads/Coeloplana_meteoris.fasta.gz) |  | 110525 |  | [Simion 2017](http://dx.doi.org/10.1016/j.cub.2017.02.031) |
| *Coeloplana meteoris* | Platyctenida | Coeloplanidae | Txome | [SRR5892574](https://www.ncbi.nlm.nih.gov/sra/SRX3058270) |  |  |  |  | [Whelan 2017](http://dx.doi.org/10.1038/s41559-017-0331-3) |
| *Vallicula multiformis* | Platyctenida | Coeloplanidae | Txome | [SRR786489](https://www.ncbi.nlm.nih.gov/sra/SRX250325) | [illumina](https://neurobase.rc.ufl.edu/pleurobrachia/download) |  |  |  | [Moroz 2014](https://doi.org/10.1038/nature13400) |
| [*Vallicula multiformis*](http://www.marinespecies.org/aphia.php?p=taxdetails&id=106403) | Platyctenida | Coeloplanidae | Txome | [SRR3407164](https://www.ncbi.nlm.nih.gov/sra/SRX1716825) | [illumina](https://bitbucket.org/wrf/ctenophore-transcriptomes/downloads/Vallicula_multiformis.fasta.gz) |  | 79493 |  | [Simion 2017](http://dx.doi.org/10.1016/j.cub.2017.02.031) |
| [*Hormiphora californensis*](http://www.marinespecies.org/aphia.php?p=taxdetails&id=559444), possibly *H. palmata* | Cydippida | Pleurobrachiidae | Txome | [SRR1992642](http://www.ncbi.nlm.nih.gov/sra/SRX1006956) | [GGLO01](https://www.ncbi.nlm.nih.gov/Traces/wgs/?val=GGLO01) |  | 75387 |  | [Francis 2015](https://doi.org/10.1371/journal.pone.0128742) |
| *Hormiphora californensis* | Cydippida | Pleurobrachiidae | Txome |  | [GHXS01](https://www.ncbi.nlm.nih.gov/Traces/wgs/?val=GHXS01) |  | 114663 |  | [Schultz 2021](https://doi.org/10.1093/g3journal/jkab302) |
| *Hormiphora californensis*, probably should be reassigned to *Pleurobrachia* | Cydippida | Pleurobrachiidae | Genome | [PRJNA576068](https://www.ncbi.nlm.nih.gov/bioproject/576068) | [Hcv1av93](https://doi.org/10.5281/zenodo.4074309) | 110.6 | 45 | 8.537 | [Schultz 2021](https://doi.org/10.1093/g3journal/jkab302) |
| [*Hormiphora palmata*](http://www.marinespecies.org/aphia.php?p=taxdetails&id=265184) | Cydippida | Pleurobrachiidae | Txome | [SRR6074513](https://www.ncbi.nlm.nih.gov/sra/SRX3215108) |  |  |  |  | [Whelan 2017](http://dx.doi.org/10.1038/s41559-017-0331-3) |
| [*Pleurobrachia bachei*](http://www.marinespecies.org/aphia.php?p=taxdetails&id=265191) | Cydippida | Pleurobrachiidae | Genome | [PRJNA213480](https://www.ncbi.nlm.nih.gov/bioproject/213480) | [NCBI](https://www.ncbi.nlm.nih.gov/Traces/wgs/AVPN01) | 156.1 | 21979 | 0.020 | [Moroz 2014](https://doi.org/10.1038/nature13400) |
| [*Pleurobrachia pileus*](http://www.marinespecies.org/aphia.php?p=taxdetails&id=106386) | Cydippida | Pleurobrachiidae | Txome | [SRR6074514](https://www.ncbi.nlm.nih.gov/sra/SRX3215107) |  |  |  |  | [Moroz 2014](https://doi.org/10.1038/nature13400) |
| [*Pukia falcata*](http://www.marinespecies.org/aphia.php?p=taxdetails&id=848354) | Cydippida | Pukiidae | Txome | [SRR5892572](https://www.ncbi.nlm.nih.gov/sra/SRX3058272) | [illumina](https://neurobase.rc.ufl.edu/pleurobrachia/download) |  | 119037 |  | [Whelan 2017](http://dx.doi.org/10.1038/s41559-017-0331-3) |
| *Callianira antarctica* |  |  | Txome | [SRR5892575](https://www.ncbi.nlm.nih.gov/sra/SRX3058269) |  |  |  |  | [Whelan 2017](http://dx.doi.org/10.1038/s41559-017-0331-3) |
| *Lobatolampea tetragona* |  |  | Txome | [SRR5892580](https://www.ncbi.nlm.nih.gov/sra/SRX3058264) |  |  |  |  | [Whelan 2017](http://dx.doi.org/10.1038/s41559-017-0331-3) |
| [*Lampea pancerina*](http://www.marinespecies.org/aphia.php?p=taxdetails&id=106372) |  |  | Txome | [SRR3407163](https://www.ncbi.nlm.nih.gov/sra/SRX1716824) | [illumina](https://bitbucket.org/wrf/ctenophore-transcriptomes/downloads/Lampea_pancerina.fasta.gz) |  | 80639 |  | [Simion 2017](http://dx.doi.org/10.1016/j.cub.2017.02.031) |
| *Lampea* sp |  |  | Txome | - | [Trinity](https://doi.org/10.6084/m9.figshare.6771635.v1) |  | 130071 |  |  |
| [*Beroe abyssicola*](http://www.marinespecies.org/aphia.php?p=taxdetails&id=265150) |  | Beroida | Txome |  | [illumina](https://neurobase.rc.ufl.edu/pleurobrachia/download) |  |  |  | [Moroz 2014](https://doi.org/10.1038/nature13400) |
| [*Beroe forskalii*](http://www.marinespecies.org/aphia.php?p=taxdetails&id=106360) |  | Beroida | Txome | [SRR6074515](https://www.ncbi.nlm.nih.gov/sra/SRX3215106) |  |  |  |  | [Whelan 2017](http://dx.doi.org/10.1038/s41559-017-0331-3) |
| *Beroe ovata* |  | Beroida | Txome | [SRR6074516](https://www.ncbi.nlm.nih.gov/sra/SRX3215105) |  |  |  |  | [Whelan 2017](http://dx.doi.org/10.1038/s41559-017-0331-3) |
| [*Bolinopsis ashleyi*](http://www.marinespecies.org/aphia.php?p=taxdetails&id=848391) |  |  | Txome | [SRR5892570](https://www.ncbi.nlm.nih.gov/sra/SRX3058274) | [illumina](https://neurobase.rc.ufl.edu/pleurobrachia/download) |  | 88514 |  | [Whelan 2017](http://dx.doi.org/10.1038/s41559-017-0331-3) |
| *Bolinopsis infundibulum* |  |  | Txome | [SRR6074521](https://www.ncbi.nlm.nih.gov/sra/SRX3215100) | [illumina](https://neurobase.rc.ufl.edu/pleurobrachia/download) |  |  |  | [Whelan 2017](http://dx.doi.org/10.1038/s41559-017-0331-3) |
| [*Cestum veneris*](http://www.marinespecies.org/aphia.php?p=taxdetails&id=106363) |  |  | Txome | [SRR3341364](https://www.ncbi.nlm.nih.gov/sra/SRX1683952) | [454](https://bitbucket.org/wrf/ctenophore-transcriptomes/downloads/Cestum_veneris.fasta.gz) |  | 15003 |  | [Simion 2017](http://dx.doi.org/10.1016/j.cub.2017.02.031) |
| [*Dryodora glandiformis*](http://www.marinespecies.org/aphia.php?p=taxdetails&id=106366) |  |  | Txome |  | [illumina](https://neurobase.rc.ufl.edu/pleurobrachia/download) |  |  |  | [Moroz 2014](https://doi.org/10.1038/nature13400) |
| [*Eurhamphaea vexilligera*](http://www.marinespecies.org/aphia.php?p=taxdetails&id=106400) |  |  | Txome | [SRR6074510](https://www.ncbi.nlm.nih.gov/sra/SRX3215111) |  |  |  |  | [Whelan 2017](http://dx.doi.org/10.1038/s41559-017-0331-3) |
| [*Mnemiopsis leidyi*](http://www.marinespecies.org/aphia.php?p=taxdetails&id=106401) |  | Bolinopsidae | Genome |  | [ML2.2](https://research.nhgri.nih.gov/mnemiopsis/download/download.cgi?dl=genome) | 155.8 | 5100 | 0.187 | [Ryan2013](https://doi.org/10.1126/science.1242592) |
| [*Ocyropsis crystallina*](http://www.marinespecies.org/aphia.php?p=taxdetails&id=265211) |  | Ocyropsidae | Txome | [SRR6074507](https://www.ncbi.nlm.nih.gov/sra/SRX3215114) |  |  |  |  | [Whelan 2017](http://dx.doi.org/10.1038/s41559-017-0331-3) |
| [*Ocyropsis crystallina* guttata](http://www.marinespecies.org/aphia.php?p=taxdetails&id=422762) |  | Ocyropsidae | Txome | [SRR6074508](https://www.ncbi.nlm.nih.gov/sra/SRX3215113) |  |  |  |  | [Whelan 2017](http://dx.doi.org/10.1038/s41559-017-0331-3) |
|  |  |  |  |  |  |  |  |  |  |

